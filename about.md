---
layout: page
title: About
permalink: /about/
---

Hi, My name is Thomas. I’m a software engineer, generally veer towards more “Fullstack” but that’s just because I’m a tinkerer
and I like puzzles, so I enjoy diving into all aspects of software development and design.

If you'd like to get in touch I can be found at:
- [Mastodon](https://fosstodon.org/@Vapourisation)
- [Hashnode](https://tpegler.hashnode.dev/)
- [Gitlab](https://gitlab.com/Vapourisation)
- [Email](mailto:vapourisation@proton.me)